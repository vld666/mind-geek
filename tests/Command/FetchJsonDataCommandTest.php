<?php
declare(strict_types=1);

namespace App\Tests\Command;

use Symfony\Bundle\FrameworkBundle\Console\Application;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\Console\Tester\CommandTester;

class FetchJsonDataCommandTest extends WebTestCase
{
    protected function setUp()
    {
        static::$kernel = static::createKernel();
        static::$kernel->boot();
    }

    /** @test */
    public function execute()
    {
        $application = new Application(static::$kernel);
        $command = $application->find('app:fetch-data');
        $command->setApplication($application);
        $commandTester = new CommandTester($command);
        $commandTester->execute(
            [
                'command'  => $command->getName(),
            ]
        );

        self::assertContains('Finished', $commandTester->getDisplay());
    }
}
