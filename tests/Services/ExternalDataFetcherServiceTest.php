<?php
declare(strict_types=1);

namespace App\Tests\Services;

use App\Services\ExternalDataFetcherService;
use GuzzleHttp\Client;
use PHPUnit\Framework\TestCase;
use Psr\Log\LoggerInterface;

class ExternalDataFetcherServiceTest extends TestCase
{
    private $guzzleClient;
    private $logger;

    public function setUp()
    {
        $this->guzzleClient = new Client();
        $this->logger = $this->createMock(LoggerInterface::class);
    }

    /** @test */
    public function fetch()
    {
        $service = new ExternalDataFetcherService($this->guzzleClient, $this->logger);
        $actual = $service->fetch();
        self::assertNotNull($actual);
    }
}
