<?php
declare(strict_types=1);

namespace App\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\Response;

class MovieControllerTest extends WebTestCase
{
    /** @test */
    public function getHomePage()
    {
        $client = static::createClient();
        //$client = $this->createClient();
        $client->request('GET', '/');

        self::assertEquals(Response::HTTP_OK, $client->getResponse()->getStatusCode());
        self::assertResponseIsSuccessful();

        self::assertContains(
            'Movie List',
            $client->getResponse()->getContent()
        );
        self::assertResponseIsSuccessful();
    }
    /** @test */
    public function getSingleMovie()
    {
        $client = $this->createClient();
        $crawler = $client->request('GET', '/movie/emperor');

        self::assertEquals(Response::HTTP_OK, $client->getResponse()->getStatusCode());
        self::assertResponseIsSuccessful();

        self::assertContains(
            'Back to movie list',
            $client->getResponse()->getContent()
        );
        self::assertResponseIsSuccessful();

        self::assertGreaterThan(0, $crawler->filter('h1')->count());
        self::assertResponseIsSuccessful();
    }
}
