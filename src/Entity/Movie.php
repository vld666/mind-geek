<?php
declare(strict_types=1);

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;


/**
 * @ORM\Entity(repositoryClass="App\Repository\MovieRepository")
 */
class Movie
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="NONE")
     * @ORM\Column(type="string", length=32, nullable=false)
     */
    private $id;

    /**
     * @ORM\Column(type="text")
     */
    private $body;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $cert;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $class;

    /**
     * @ORM\Column(type="integer")
     */
    private $duration;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $headline;

    /**
     * @ORM\Column(type="date_immutable")
     */
    private $lastUpdated;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $quote;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $rating;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $reviewAuthor;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $skyGoId;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $skyGoUrl;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $sum;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $synopsis;

    /**
     * @ORM\Column(type="text")
     */
    private $url;

    /**
     * @ORM\Column(type="string", length=4)
     */
    private $year;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\CardImage", mappedBy="movie")
     */
    private $cardImages;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Cast", inversedBy="movies")
     */
    private $cast;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Director", inversedBy="movies")
     */
    private $directors;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Gallery", mappedBy="movie")
     */
    private $galleries;

    /**
     * @ORM\Column(type="simple_array", length=255, nullable=true)
     */
    private $genres;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Video", mappedBy="movie")
     */
    private $videos;

    /**
     * @ORM\Column(type="array", length=255, nullable=true)
     */
    private $viewingWindow;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\KeyArtImage", inversedBy="movies")
     */
    private $keyArtImages;

    public function __construct()
    {
        $this->cardImages = new ArrayCollection();
        $this->cast = new ArrayCollection();
        $this->directors = new ArrayCollection();
        $this->galleries = new ArrayCollection();
        $this->videos = new ArrayCollection();
        $this->keyArtImages = new ArrayCollection();
    }

    public function getId(): string
    {
        return $this->id;
    }

    public function setId(string $id): self
    {
        $this->id = $id;

        return $this;
    }

    public function getBody(): ?string
    {
        return $this->body;
    }

    public function setBody(string $body): self
    {
        $this->body = $body;

        return $this;
    }

    public function getCert(): ?string
    {
        return $this->cert;
    }

    public function setCert(string $cert): self
    {
        $this->cert = $cert;

        return $this;
    }

    public function getClass(): ?string
    {
        return $this->class;
    }

    public function setClass(string $class): self
    {
        $this->class = $class;

        return $this;
    }

    public function getDuration(): ?int
    {
        return $this->duration;
    }

    public function setDuration(int $duration): self
    {
        $this->duration = $duration;

        return $this;
    }

    public function getHeadline(): ?string
    {
        return $this->headline;
    }

    public function setHeadline(string $headline): self
    {
        $this->headline = $headline;

        return $this;
    }

    public function getLastUpdated(): ?\DateTimeImmutable
    {
        return $this->lastUpdated;
    }

    public function setLastUpdated($lastUpdated): self
    {
        $this->lastUpdated = \DateTimeImmutable::createFromFormat('Y-m-d',$lastUpdated);

        return $this;
    }

    public function getQuote(): ?string
    {
        return $this->quote;
    }

    public function setQuote(?string $quote): self
    {
        $this->quote = $quote;

        return $this;
    }

    public function getRating(): ?int
    {
        return $this->rating;
    }

    public function setRating(?int $rating): self
    {
        $this->rating = $rating;

        return $this;
    }

    public function getReviewAuthor(): ?string
    {
        return $this->reviewAuthor;
    }

    public function setReviewAuthor(?string $reviewAuthor): self
    {
        $this->reviewAuthor = $reviewAuthor;

        return $this;
    }

    public function getSkyGoId(): ?string
    {
        return $this->skyGoId;
    }

    public function setSkyGoId(?string $skyGoId): self
    {
        $this->skyGoId = $skyGoId;

        return $this;
    }

    public function getSkyGoUrl(): ?string
    {
        return $this->skyGoUrl;
    }

    public function setSkyGoUrl(?string $skyGoUrl): self
    {
        $this->skyGoUrl = $skyGoUrl;

        return $this;
    }

    public function getSum(): ?string
    {
        return $this->sum;
    }

    public function setSum(string $sum): self
    {
        $this->sum = $sum;

        return $this;
    }

    public function getSynopsis(): ?string
    {
        return $this->synopsis;
    }

    public function setSynopsis(?string $synopsis): self
    {
        $this->synopsis = $synopsis;

        return $this;
    }

    public function getUrl(): ?string
    {
        return $this->url;
    }

    public function setUrl(string $url): self
    {
        $this->url = $url;

        return $this;
    }

    public function getYear(): ?string
    {
        return $this->year;
    }

    public function setYear(string $year): self
    {
        $this->year = $year;

        return $this;
    }

    public function getGenres(): ?array
    {
        return $this->genres;
    }

    public function setGenres(?array $genres): self
    {
        $this->genres = $genres;
        return $this;
    }

    /**
     * @return Collection|CardImage[]
     */
    public function getCardImages(): Collection
    {
        return $this->cardImages;
    }

    public function addCardImage(CardImage $cardImage): self
    {
        if (!$this->cardImages->contains($cardImage)) {
            $this->cardImages[] = $cardImage;
            $cardImage->setMovie($this);
        }

        return $this;
    }

    public function removeCardImage(CardImage $cardImage): self
    {
        if ($this->cardImages->contains($cardImage)) {
            $this->cardImages->removeElement($cardImage);
            // set the owning side to null (unless already changed)
            if ($cardImage->getMovie() === $this) {
                $cardImage->setMovie(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Cast[]
     */
    public function getCast(): Collection
    {
        return $this->cast;
    }

    public function addCast(Cast $cast): self
    {
        if (!$this->cast->contains($cast)) {
            $this->cast[] = $cast;
        }

        return $this;
    }

    public function removeCast(Cast $cast): self
    {
        if ($this->cast->contains($cast)) {
            $this->cast->removeElement($cast);
        }

        return $this;
    }

    /**
     * @return Collection|Director[]
     */
    public function getDirectors(): Collection
    {
        return $this->directors;
    }

    public function addDirector(Director $director): self
    {
        if (!$this->directors->contains($director)) {
            $this->directors[] = $director;
        }

        return $this;
    }

    public function removeDirector(Director $director): self
    {
        if ($this->directors->contains($director)) {
            $this->directors->removeElement($director);
        }

        return $this;
    }

    /**
     * @return Collection|Video[]
     */
    public function getVideos(): Collection
    {
        return $this->videos;
    }

    public function addVideos(Video $video): self
    {
        if (!$this->videos->contains($video)) {
            $this->videos[] = $video;
        }

        return $this;
    }

    public function removeVideo(Video $video): self
    {
        if ($this->videos->contains($video)) {
            $this->videos->removeElement($video);
        }

        return $this;
    }

    /**
     * @return Collection|KeyArtImage[]
     */
    public function getKeyArtImages(): Collection
    {
        return $this->keyArtImages;
    }

    public function addKeyArtImage(KeyArtImage $keyArtImage): self
    {
        if (!$this->keyArtImages->contains($keyArtImage)) {
            $this->keyArtImages[] = $keyArtImage;
        }

        return $this;
    }

    public function removeKeyArtImage(KeyArtImage $keyArtImage): self
    {
        if ($this->keyArtImages->contains($keyArtImage)) {
            $this->keyArtImages->removeElement($keyArtImage);
        }

        return $this;
    }

    public function getGalleries()
    {
        return $this->galleries;
    }

    public function addGallery(Gallery $gallery): self
    {
        if (!$this->galleries->contains($gallery)) {
            $this->galleries[] = $gallery;
        }

        return $this;
    }

    public function removeGallery(Gallery $gallery): self
    {
        if ($this->galleries->contains($gallery)) {
            $this->galleries->removeElement($gallery);
        }

        return $this;
    }

    public function getViewingWindow()
    {
        return $this->viewingWindow;
    }

    public function setViewingWindow($viewingWindow)
    {
        $this->viewingWindow = $viewingWindow;
        return $this;
    }
}
