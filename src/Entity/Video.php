<?php
declare(strict_types=1);

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\VideoRepository")
 */
class Video
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $title;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $type;

    /**
     * @ORM\Column(type="text")
     */
    private $url;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\VideoAlternative", mappedBy="video")
     */
    private $alternatives;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Movie", inversedBy="videos")
     */
    private $movie;

    public function __construct()
    {
        $this->alternatives = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getType(): ?string
    {
        return $this->type;
    }

    public function setType(string $type): self
    {
        $this->type = $type;

        return $this;
    }

    public function getUrl(): ?string
    {
        return $this->url;
    }

    public function setUrl(string $url): self
    {
        $this->url = $url;

        return $this;
    }

    public function getMovie(): Movie
    {
        return $this->movie;
    }

    public function setMovie(Movie $movie): self
    {
        $this->movie = $movie;
        return $this;
    }

    /**
     * @return Collection|VideoAlternative[]
     */
    public function getAlternatives(): Collection
    {
        return $this->alternatives;
    }

    public function addAlternative(VideoAlternative $alternative): self
    {
        if (!$this->alternatives->contains($alternative)) {
            $this->alternatives[] = $alternative;
            $alternative->setVideo($this);
        }

        return $this;
    }

    public function removeAlternative(VideoAlternative $alternative): self
    {
        if ($this->alternatives->contains($alternative)) {
            $this->alternatives->removeElement($alternative);
            // set the owning side to null (unless already changed)
            if ($alternative->getVideo() === $this) {
                $alternative->setVideo(null);
            }
        }

        return $this;
    }
}
