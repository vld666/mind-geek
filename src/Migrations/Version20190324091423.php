<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190324091423 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE genre');
        $this->addSql('ALTER TABLE viewing_window ADD movie_id VARCHAR(32) DEFAULT NULL');
        $this->addSql('ALTER TABLE viewing_window ADD CONSTRAINT FK_A15E10E48F93B6FC FOREIGN KEY (movie_id) REFERENCES movie (id)');
        $this->addSql('CREATE INDEX IDX_A15E10E48F93B6FC ON viewing_window (movie_id)');
        $this->addSql('ALTER TABLE movie ADD viewing_window TINYTEXT DEFAULT NULL COMMENT \'(DC2Type:array)\'');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE genre (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL COLLATE utf8mb4_unicode_ci, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('ALTER TABLE movie DROP viewing_window');
        $this->addSql('ALTER TABLE viewing_window DROP FOREIGN KEY FK_A15E10E48F93B6FC');
        $this->addSql('DROP INDEX IDX_A15E10E48F93B6FC ON viewing_window');
        $this->addSql('ALTER TABLE viewing_window DROP movie_id');
    }
}
