<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190323101152 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE viewing_window (id INT AUTO_INCREMENT NOT NULL, title VARCHAR(255) NOT NULL, start_date DATE NOT NULL COMMENT \'(DC2Type:date_immutable)\', way_to_watch VARCHAR(255) NOT NULL, end_date DATE NOT NULL COMMENT \'(DC2Type:date_immutable)\', PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE cast (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE gallery (id CHAR(32) NOT NULL, movie_id VARCHAR(32) DEFAULT NULL, title VARCHAR(255) NOT NULL, url LONGTEXT NOT NULL, thumbnail_url LONGTEXT NOT NULL, INDEX IDX_472B783A8F93B6FC (movie_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE movie (id VARCHAR(32) NOT NULL, body LONGTEXT NOT NULL, cert VARCHAR(100) NOT NULL, class VARCHAR(255) NOT NULL, duration INT NOT NULL, headline VARCHAR(255) NOT NULL, last_updated DATE NOT NULL COMMENT \'(DC2Type:date_immutable)\', quote LONGTEXT NOT NULL, rating INT NOT NULL, review_author VARCHAR(255) NOT NULL, sky_go_id VARCHAR(255) DEFAULT NULL, sky_go_url LONGTEXT NOT NULL, sum VARCHAR(255) NOT NULL, synopsis LONGTEXT DEFAULT NULL, url LONGTEXT NOT NULL, year VARCHAR(4) NOT NULL, genres TINYTEXT DEFAULT NULL COMMENT \'(DC2Type:simple_array)\', PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE movie_cast (movie_id VARCHAR(32) NOT NULL, cast_id INT NOT NULL, INDEX IDX_E1DE98FB8F93B6FC (movie_id), INDEX IDX_E1DE98FB27B5E40F (cast_id), PRIMARY KEY(movie_id, cast_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE movie_director (movie_id VARCHAR(32) NOT NULL, director_id INT NOT NULL, INDEX IDX_C266487D8F93B6FC (movie_id), INDEX IDX_C266487D899FB366 (director_id), PRIMARY KEY(movie_id, director_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE movie_key_art_image (movie_id VARCHAR(32) NOT NULL, key_art_image_id INT NOT NULL, INDEX IDX_67B1A0718F93B6FC (movie_id), INDEX IDX_67B1A0718B2C2252 (key_art_image_id), PRIMARY KEY(movie_id, key_art_image_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE key_art_image (id INT AUTO_INCREMENT NOT NULL, url VARCHAR(255) NOT NULL, h VARCHAR(255) NOT NULL, w VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE card_image (id INT AUTO_INCREMENT NOT NULL, movie_id VARCHAR(32) DEFAULT NULL, url VARCHAR(255) NOT NULL, h VARCHAR(255) NOT NULL, w VARCHAR(255) NOT NULL, INDEX IDX_FD09F5998F93B6FC (movie_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE genre (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE video (id INT AUTO_INCREMENT NOT NULL, movie_id VARCHAR(32) DEFAULT NULL, title VARCHAR(255) NOT NULL, type VARCHAR(255) NOT NULL, url LONGTEXT NOT NULL, INDEX IDX_7CC7DA2C8F93B6FC (movie_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE director (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE video_alternative (id INT AUTO_INCREMENT NOT NULL, video_id INT DEFAULT NULL, quality VARCHAR(255) NOT NULL, url LONGTEXT NOT NULL, INDEX IDX_2C7DEC5B29C1004E (video_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE gallery ADD CONSTRAINT FK_472B783A8F93B6FC FOREIGN KEY (movie_id) REFERENCES movie (id)');
        $this->addSql('ALTER TABLE movie_cast ADD CONSTRAINT FK_E1DE98FB8F93B6FC FOREIGN KEY (movie_id) REFERENCES movie (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE movie_cast ADD CONSTRAINT FK_E1DE98FB27B5E40F FOREIGN KEY (cast_id) REFERENCES cast (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE movie_director ADD CONSTRAINT FK_C266487D8F93B6FC FOREIGN KEY (movie_id) REFERENCES movie (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE movie_director ADD CONSTRAINT FK_C266487D899FB366 FOREIGN KEY (director_id) REFERENCES director (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE movie_key_art_image ADD CONSTRAINT FK_67B1A0718F93B6FC FOREIGN KEY (movie_id) REFERENCES movie (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE movie_key_art_image ADD CONSTRAINT FK_67B1A0718B2C2252 FOREIGN KEY (key_art_image_id) REFERENCES key_art_image (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE card_image ADD CONSTRAINT FK_FD09F5998F93B6FC FOREIGN KEY (movie_id) REFERENCES movie (id)');
        $this->addSql('ALTER TABLE video ADD CONSTRAINT FK_7CC7DA2C8F93B6FC FOREIGN KEY (movie_id) REFERENCES movie (id)');
        $this->addSql('ALTER TABLE video_alternative ADD CONSTRAINT FK_2C7DEC5B29C1004E FOREIGN KEY (video_id) REFERENCES video (id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE movie_cast DROP FOREIGN KEY FK_E1DE98FB27B5E40F');
        $this->addSql('ALTER TABLE gallery DROP FOREIGN KEY FK_472B783A8F93B6FC');
        $this->addSql('ALTER TABLE movie_cast DROP FOREIGN KEY FK_E1DE98FB8F93B6FC');
        $this->addSql('ALTER TABLE movie_director DROP FOREIGN KEY FK_C266487D8F93B6FC');
        $this->addSql('ALTER TABLE movie_key_art_image DROP FOREIGN KEY FK_67B1A0718F93B6FC');
        $this->addSql('ALTER TABLE card_image DROP FOREIGN KEY FK_FD09F5998F93B6FC');
        $this->addSql('ALTER TABLE video DROP FOREIGN KEY FK_7CC7DA2C8F93B6FC');
        $this->addSql('ALTER TABLE movie_key_art_image DROP FOREIGN KEY FK_67B1A0718B2C2252');
        $this->addSql('ALTER TABLE video_alternative DROP FOREIGN KEY FK_2C7DEC5B29C1004E');
        $this->addSql('ALTER TABLE movie_director DROP FOREIGN KEY FK_C266487D899FB366');
        $this->addSql('DROP TABLE viewing_window');
        $this->addSql('DROP TABLE cast');
        $this->addSql('DROP TABLE gallery');
        $this->addSql('DROP TABLE movie');
        $this->addSql('DROP TABLE movie_cast');
        $this->addSql('DROP TABLE movie_director');
        $this->addSql('DROP TABLE movie_key_art_image');
        $this->addSql('DROP TABLE key_art_image');
        $this->addSql('DROP TABLE card_image');
        $this->addSql('DROP TABLE genre');
        $this->addSql('DROP TABLE video');
        $this->addSql('DROP TABLE director');
        $this->addSql('DROP TABLE video_alternative');
    }
}
