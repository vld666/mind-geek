<?php
declare(strict_types=1);

namespace App\Repository;

use App\Entity\KeyArtImage;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method KeyArtImage|null find($id, $lockMode = null, $lockVersion = null)
 * @method KeyArtImage|null findOneBy(array $criteria, array $orderBy = null)
 * @method KeyArtImage[]    findAll()
 * @method KeyArtImage[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class KeyArtImageRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, KeyArtImage::class);
    }
}
