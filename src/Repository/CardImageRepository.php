<?php
declare(strict_types=1);

namespace App\Repository;

use App\Entity\CardImage;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method CardImage|null find($id, $lockMode = null, $lockVersion = null)
 * @method CardImage|null findOneBy(array $criteria, array $orderBy = null)
 * @method CardImage[]    findAll()
 * @method CardImage[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CardImageRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, CardImage::class);
    }
}
