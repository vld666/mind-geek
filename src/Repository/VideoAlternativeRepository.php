<?php
declare(strict_types=1);

namespace App\Repository;

use App\Entity\VideoAlternative;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method VideoAlternative|null find($id, $lockMode = null, $lockVersion = null)
 * @method VideoAlternative|null findOneBy(array $criteria, array $orderBy = null)
 * @method VideoAlternative[]    findAll()
 * @method VideoAlternative[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class VideoAlternativeRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, VideoAlternative::class);
    }
}
