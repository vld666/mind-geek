<?php
declare(strict_types=1);

namespace App\Command;

use App\Services\Dto\EntitiesCreatorRequest;
use App\Services\EntitiesCreatorService;
use App\Services\ExternalDataFetcherService;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Helper\ProgressBar;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

final class FetchJsonDataCommand extends Command
{
    protected static $defaultName = 'app:fetch-data';

    private $entityManager;
    private $fetcherService;
    private $entitiesCreatorService;

    public function __construct
    (
        EntityManagerInterface $entityManager,
        ExternalDataFetcherService $fetcherService,
        EntitiesCreatorService $entitiesCreatorService
    ) {
        parent::__construct();
        $this->entityManager = $entityManager;
        $this->fetcherService = $fetcherService;
        $this->entitiesCreatorService = $entitiesCreatorService;
    }

    protected function configure()
    {
        $this->setDescription('Populate database with data from URL');
    }

    protected function execute(InputInterface $input, OutputInterface $output): void
    {
        $output->writeln(['Starting to fetch the data']);
        $output->writeln(['This will last some time. Grab a beer and wait!']);
        $contents = $this->fetcherService->fetch();
        $contents = utf8_encode($contents);
        $data = json_decode($contents);
        $progressBar = new ProgressBar($output, count($data));
        $progressBar->setFormat('debug');
        $progressBar->start();
        $this->entitiesCreatorService->execute(new EntitiesCreatorRequest($data), $progressBar);
        $progressBar->finish();
        $output->writeln(['Finished']);
    }
}
