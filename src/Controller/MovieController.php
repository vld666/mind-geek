<?php
declare(strict_types=1);

namespace App\Controller;

use App\Entity\Movie;
use Doctrine\ORM\EntityManagerInterface;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

final class MovieController extends AbstractController
{
    /**
     * @Route("/", name="homepage")
     * @param Request $request
     * @param PaginatorInterface $paginator
     * @param EntityManagerInterface $entityManager
     * @return Response
     */
    public function listAction(Request $request, PaginatorInterface $paginator, EntityManagerInterface $entityManager): Response
    {
        $moviesRepository = $entityManager->getRepository(Movie::class);
        $moviesQuery = $moviesRepository->createQueryBuilder('m')
            ->addOrderBy('m.lastUpdated', 'DESC')->getQuery();

        $pagination = $paginator->paginate(
            $moviesQuery,
            $request->query->getInt('page', 1),
            10 /*limit per page*/
        );

        return $this->render('movies/list.html.twig', ['pagination' => $pagination]);
    }

    /**
     * @Route("/movie/{url}", name="movie_show")
     * @param Request $request
     * @param EntityManagerInterface $entityManager
     * @return Response
     */
    public function showAction(Request $request, EntityManagerInterface $entityManager)
    {
        $movie  = $entityManager->getRepository(Movie::class)->findOneBy([
            'url' => $request->attributes->get('url')
        ]);

        return $this->render('movies/show.html.twig', ['movie' => $movie]);
    }
}
