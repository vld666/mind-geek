<?php
declare(strict_types=1);

namespace App\Services;

use App\Entity\CardImage;
use App\Entity\Cast;
use App\Entity\Director;
use App\Entity\Gallery;
use App\Entity\KeyArtImage;
use App\Entity\Movie;
use App\Entity\Video;
use App\Entity\VideoAlternative;
use App\Repository\CastRepository;
use App\Repository\DirectorsRepository;
use App\Repository\MovieRepository;
use App\Services\Dto\EntitiesCreatorRequest;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Console\Helper\ProgressBar;
use Symfony\Contracts\HttpClient\HttpClientInterface;

final class EntitiesCreatorService
{
    private $movieRepo;
    private $directorsRepo;
    private $castRepo;
    private $em;
    private $client;

    /**
     * EntitiesCreatorService constructor.
     * @param MovieRepository $movieRepository
     * @param DirectorsRepository $directorsRepository
     * @param CastRepository $castRepository
     * @param EntityManagerInterface $persistManagerService
     * @param HttpClientInterface $client
     */
    public function __construct(
        MovieRepository $movieRepository,
        DirectorsRepository $directorsRepository,
        CastRepository $castRepository,
        EntityManagerInterface $persistManagerService,
        HttpClientInterface $client
    )
    {
        $this->movieRepo = $movieRepository;
        $this->directorsRepo = $directorsRepository;
        $this->castRepo = $castRepository;
        $this->em = $persistManagerService;
        $this->client = $client;
    }

    /**
     * @param EntitiesCreatorRequest|null $request
     * @param ProgressBar $progressBar
     * @throws \Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface
     */
    public function execute(?EntitiesCreatorRequest $request, ProgressBar $progressBar): void
    {
        $data = $request->getData();
        if (!empty($data)) {
            foreach ($data as $entry) {
                if ($this->movieRepo->findOneBy(['id' => $entry->id])) {
                    continue;
                }
                $movie = $this->createMovie($entry);
                $this->createDirectors($entry, $movie, $item);
                $this->createCast($entry, $movie, $item);
                $this->createCardImages($entry, $movie, $item, $img);
                $this->createKeyArtImages($entry, $movie, $item, $img);
                $this->createVideos($entry, $movie, $item);
                $this->createGalleries($entry, $movie, $item);

                $this->em->persist($movie);
                $this->em->flush();
                $progressBar->advance(); // counting sleepless hours
            }
        }
    }

    /**
     * @param $entry
     * @return Movie
     */
    private function createMovie($entry): Movie
    {
        $movie = new Movie();
        if (isset($entry->headline)) {
            $movie->setHeadline($entry->headline);
        }
        if (isset($entry->synopsis)) {
            $movie->setSynopsis($entry->synopsis);
        }
        if (isset($entry->quote)) {
            $movie->setQuote($entry->quote);
        }
        if (isset($entry->body)) {
            $movie->setBody($entry->body);
        }
        if (isset($entry->id)) {
            $movie->setId($entry->id);
        }

        if (isset($entry->url)) {
            $movie->setUrl($entry->url);
        }
        if (isset($entry->class)) {
            $movie->setClass($entry->class);
        }
        if (isset($entry->cert)) {
            $movie->setCert($entry->cert);
        }
        if (isset($entry->duration)) {
            $movie->setDuration($entry->duration);
        }
        if (isset($entry->rating)) {
            $movie->setRating($entry->rating);
        }
        if (isset($entry->year)) {
            $movie->setYear($entry->year);
        }
        if (isset($entry->skyGoId)) {
            $movie->setSkyGoId($entry->skyGoId);
        }
        if (isset($entry->skyGoUrl)) {
            $movie->setSkyGoUrl($entry->skyGoUrl);
        }
        if (isset($entry->sum)) {
            $movie->setSum($entry->sum);
        }
        if (isset($entry->lastUpdated)) {
            $movie->setLastUpdated($entry->lastUpdated);
        }
        if (isset($entry->headline)) {
            $string = $entry->headline;
            $string = str_replace(" ", "-", $string);
            $string = str_replace(":", "", $string);
            $string = strtolower($string);
            $movie->setUrl($string);
        }
        if (isset($entry->reviewAuthor)) {
            $movie->setReviewAuthor($entry->reviewAuthor);
        }
        if (isset($entry->genres)) {
            $movie->setGenres($entry->genres);
        }
        if (isset($entry->viewingWindow)) {
            $movie->setViewingWindow($entry->viewingWindow);
        }

        return $movie;
    }

    /**
     * @param $entry
     * @param Movie $movie
     * @param $item
     */
    private function createDirectors($entry, Movie $movie, &$item): void
    {
        if (isset($entry->directors)) {
            foreach ($entry->directors as $item) {
                $director = $this->directorsRepo->findOneBy(['name' => $item->name]);
                if (!$director) {
                    $director = new Director();
                    $director->setName($item->name);

                    $this->em->persist($director);
                }
                $movie->addDirector($director);
            }
        }
    }

    /**
     * @param $entry
     * @param Movie $movie
     * @param $item
     */
    private function createCast($entry, Movie $movie, &$item): void
    {
        if (isset($entry->cast)) {
            foreach ($entry->cast as $item) {
                $castMember = $this->castRepo->findOneBy(['name' => $item->name]);
                if (!$castMember) {
                    $castMember = new Cast();
                    $castMember->setName($item->name);

                    $this->em->persist($castMember);
                }
                $movie->addCast($castMember);
            }
        }
    }

    /**
     * @param $entry
     * @param Movie $movie
     * @param $item
     * @param $img
     * @throws \Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface
     */
    private function createCardImages($entry, Movie $movie, &$item, &$img): void
    {
        if (isset($entry->cardImages)) {
            foreach ($entry->cardImages as $item) {
                $image = $this->saveContentFromUrl($item->url);
                if (empty($image)) {
                    continue;
                }
                $img = new CardImage();
                $img->setUrl($image);
                if (isset($item->h)) $img->setH($item->h);
                if (isset($item->w)) $img->setW($item->w);
                $this->em->persist($img);
                $movie->addCardImage($img);
            }
        }
    }

    /**
     * @param $entry
     * @param Movie $movie
     * @param $item
     * @param $img
     * @throws \Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface
     */
    private function createKeyArtImages($entry, Movie $movie, &$item, &$img): void
    {
        if (isset($entry->keyArtImages)) {
            foreach ($entry->keyArtImages as $item) {
                $image = $this->saveContentFromUrl($item->url);
                if (empty($image)) {
                    continue;
                }
                $img = new KeyArtImage();
                $img->setUrl($image);
                if (isset($item->h)) $img->setH($item->h);
                if (isset($item->w)) $img->setW($item->w);
                $this->em->persist($img);
                $movie->addKeyArtImage($img);
            }
        }
    }

    /**
     * @param $item
     * @param Video $video
     */
    private function createVideoAlternatives($item, Video $video): void
    {
        if (isset($item->alternatives)) {
            foreach ($item->alternatives as $alt) {
                $alternative = new VideoAlternative();
                $alternative->setQuality($alt->quality);
                $alternative->setUrl($alt->url);
                $alternative->setVideo($video);
                $this->em->persist($alternative);
            }
        }
    }

    /**
     * @param $entry
     * @param Movie $movie
     * @param $item
     */
    private function createVideos($entry, Movie $movie, &$item): void
    {
        if (isset($entry->videos)) {
            foreach ($entry->videos as $item) {
                $video = new Video();
                $video->setTitle($item->title);
                $video->setType($item->type);
                $video->setUrl($item->url);
                $video->setMovie($movie);
                $this->em->persist($video);

                $this->createVideoAlternatives($item, $video);
            }
        }
    }

    /**
     * @param $entry
     * @param Movie $movie
     * @param $item
     */
    private function createGalleries($entry, Movie $movie, &$item): void
    {
        if (isset($entry->galleries)) {
            foreach ($entry->galleries as $item) {
                $gallery = new Gallery();
                if (isset($item->title)) $gallery->setTitle($item->title);
                if (!empty($item->url)) $gallery->setUrl($item->url);
                if (isset($item->thumbnailUrl)) $gallery->setThumbnailUrl($item->thumbnailUrl);
                if (isset($item->id)) $gallery->setId($item->id);
                $this->em->persist($gallery);
                $movie->addGallery($gallery);
                $gallery->setMovie($movie);
            }
        }
    }

    /**
     * @param $url
     * @return string|null
     * @throws \Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface
     */
    private function saveContentFromUrl($url)
    {
        $ext = pathinfo($url, PATHINFO_EXTENSION);
        if (empty($url) || !in_array($ext, ['jpg', 'png', 'jpeg'])) {
            return $url;
        }
        if (!in_array($ext, ['jpg', 'png', 'jpeg'], true)) {
            return $url;
        }

        $uploadPath = $this->getDirectory();
        $sitePath = 'images/';
        $filename = basename($url);

        if (file_exists($uploadPath . $filename)) {
            return $sitePath . $filename;
        }

        $responses = $this->client->request('GET', $url, ['timeout' => 3.5]);
        if (Response::HTTP_OK === $responses->getStatusCode()) {
            $resource = fopen($uploadPath . $filename, 'w');
            foreach ($this->client->stream($responses) as $chunk) {
                fwrite($resource, $chunk->getContent());
            }
            fclose($resource);

            return $sitePath . $filename;
        }

        return null;
    }

    /**
     * @return string
     */
    private function getDirectory()
    {
        // TODO: move this to a constant in config
        $uploadPath = __DIR__ . '/../../public/images/';
        if (!file_exists($uploadPath) || !is_dir($uploadPath)) {
            mkdir($uploadPath, 0777, true);
        }

        return $uploadPath;
    }
}
