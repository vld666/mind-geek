<?php
declare(strict_types=1);

namespace App\Services;

use GuzzleHttp\Client as Client;
use GuzzleHttp\Exception\ClientException;
use Psr\Log\LoggerInterface;

final class ExternalDataFetcherService
{
    public const EXTERNAL_URL = 'https://mgtechtest.blob.core.windows.net/files/showcase.json';

    private $guzzleClient;
    private $logger;

    public function __construct(Client $guzzleClient, LoggerInterface $logger)
    {
        $this->guzzleClient = $guzzleClient;
        $this->logger = $logger;
    }

    public function fetch(): ?string
    {
        $response = '';

        try {
            $response  = $this->guzzleClient->get(self::EXTERNAL_URL);
        } catch (ClientException $clientException) {
            $this->logger->error(sprintf('Guzzle exception %s', $clientException->getMessage()));
        } catch (\Throwable $exception) {
            $this->logger->error(sprintf('General error %s', $exception->getMessage()));
        }

        return $response->getBody()->getContents();
    }
}
