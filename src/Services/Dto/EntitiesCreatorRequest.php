<?php
declare(strict_types=1);

namespace App\Services\Dto;

final class EntitiesCreatorRequest
{
    private $data;

    public function __construct($data)
    {
        $this->data = $data;
    }

    public function getData()
    {
        return $this->data;
    }
}
