**Business requirements**

You will need to write a program that downloads all the items in https://mgtechtest.blob.core.windows.net/files/showcase.json and cache images within each asset. To make it efficient, it is desired to only call the URLs in the JSON file only once. Demonstrate, by using a framework of your choice, your software architectural skills. How you use the framework will be highly important in the evaluation.

How you display the feed and how many layers/pages you use is up to you, but please ensure that we can see the complete list and the details of every item. You will likely hitsome road blocks and errors along the way, please use your own initiative to deal with these issues, it�s part of the test.

Please ensure all code is tested before sending it back, it would be good to also see unit tests too. Ideally, alongside supplying the code base and all packages/libraries required to deploy, you will also have to supply deployment instructions too.


**Technical Requirements**

- `PHP 7.1.3 or above`
- `MySQL 5.7 or above`
- `composer`
- `add dev.mindgeek in your /etc/hosts file (depending on apache or nginx)`


**Project setup**
- `git clone git@bitbucket.org:vld666/mindgeek.git`

- cd mindgeek/

- `composer install`

*Create database:*

- `php bin/console doctrine:database:create`

*Run migrations*

- `php bin/console --no-interaction doctrine:migrations:migrate`

*Run the cron to populate the database*

- `php bin/console app:fetch-data`

*Optionally run the tests*

- `php bin/phpunit`